@extends('layouts.template')

@section('content')
<div class="x_panel">
    <div class="x_title">
        <h2>Tambah Data Siswa</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <!-- start form for validation -->
        <form action="{{ route('siswa.store') }}" method="post">
            @csrf
            <label for="name"> Name * </label>
                <input type="text" id="name" class="form-control" name="name" required="" placeholder="Masukkan Nama Siswa">
            <br>
            <label for="nis">NIS * </label>
                <input type="number" id="nis" class="form-control" name="nis" required="" placeholder="Masukkan NIS">
            <br>
            <label class="mr-4">Gender *</label><br>
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" name="gender" id="gender1" value="laki-laki" class="custom-control-input" >
                    <label class="custom-control-label" for="gender1">Laki-Laki</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" name="gender" id="gender2" value="perempuan" class="custom-control-input" >
                    <label class="custom-control-label" for="gender2">Perempuan</label>
                </div>
            <br>
            <br>
            <div class="form-group">
                <label for="tanggal_lahir">Tanggal Lahir *</label>
                <input type="date" name="tanggal_lahir" class="form-control" id="tanggal_lahir" placeholder="Pilih Tanggal Lahir">
            </div>

            <button type="submit" class="btn btn-danger">Simpan</button>
        </form>
    <!-- end form for validations -->
    </div>
</div>
@endsection
