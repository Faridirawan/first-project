@extends('layouts.template')

@section('content')
<div class="col-md-12 col-sm-6">
  <div class="row">
    <div class="x_panel">
      <div class="x_title">
        <h2>Data Siswa</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li>
              <a href="/siswa/tambah">
                <button type="button" class="btn btn-primary btn-sm">Tambah</button>
              </a>
            </li>
          </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table class="table table-striped">
          <thead>
            <tr style="text-align: center;">
                <th scope="col">No</th>
                <th scope="col">Nama</th>
                <th scope="col">NIS</th>
                <th scope="col">Gender</th>
                <th scope="col">Tanggal Lahir</th>
                <th scope="col">Pilihan</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($siswa as $i => $item)
            <tr style="text-align: center;"> 
                <td>{{ $i+1 }}</td>
                <td>{{ $item->name }}</td>
                <td>{{ $item->nis }}</td>
                <td>{{ $item->gender}}</td>
                <td>{{ $item->tanggal_lahir }}</td>
                <td>
                  <form action="{{ route('siswa.destroy',$item->id) }}" method="post">
                  <a href="{{ route('siswa.edit',$item->id) }}">
                    <button type="button" class="btn btn-success">Ubah</button>
                  </a>
                  @csrf 
                  @method('delete')
                      <button type="submit" class="btn btn-danger" onclick="return confirm('Yakin ingin dihapus ?' )">Hapus</button>
                    <a href="{{ route('siswa.show', $item->id) }}">
                      <button type="button" class="btn btn-primary">Detail</button>
                    </a>
                  </form>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>  
@endsection
