@extends('layouts.template')

@section('content')
<div class="x_panel">
    <div class="x_title">
        <h2>Tambah Data Buku</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <!-- start form for validation -->
        <form action="{{ route('books.store') }}" method="post">
            @csrf
                <div class="form-row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="title">Judul Buku</label>
                            <input type="text" name="title" class="form-control"
                             id="title" placeholder="Masukkan Nama Buku">
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="category">Kategori</label>
                            <select class="form-control" id="category" name="category">
                                <option>chose...</option>
                                @foreach ($category as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="year">Tahun</label>
                        <input type="number" class="form-control" id="year" 
                        name="year" placeholder="Masukan Tahun Terbit Baru">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="author">Penulis</label>
                        <input type="text" class="form-control" id="author" 
                        name="author" placeholder="Masukan Penulis">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="publisher">Penerbit</label>
                        <input type="text" class="form-control" id="publisher" 
                        name="publisher" placeholder="Masukan Penerbit">
                    </div>
                    </div>
                <button type="submit" class="btn btn-danger">Simpan</button>
        </form>
    <!-- end form for validations -->
    </div>
</div>
@endsection
