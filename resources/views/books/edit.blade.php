@extends('layouts.template')

@section('content')
<div class="x_panel">
    <div class="x_title">
        <h2>Ubah Data Buku</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <!-- start form for validation -->
        <form action="{{ route('books.update', $book->id) }}" method="post">
            @csrf
            @method('put')
                <div class="form-row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="title">Judul Buku</label>
                            <input type="text" name="title" class="form-control" 
                            id="title" placeholder="Masukkan Nama" value="{{ $book->title }}">
                        </div>
                        </div>
                        <div class="form-group col md-6">
                            <label for="category">Kategori</label>
                            <select class="form-control" name="category" 
                            id="category" >
                            <option>Choose...</option>
                            @foreach($category as $item)
                            <option value="{{ $item->id }}" {{ $book->book_category_id ==
                             $item->id ? 'selected' : ''}}>{{ $item->name }}</option>
                            @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="author">Penulis</label>
                            <input type="text" name="author" class="form-control" 
                            id="author" placeholder="Masukkan Nama" value="{{ $book->author }}">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="publisher">Penerbit</label>
                            <input type="text" name="publisher" class="form-control" 
                            id="publisher" placeholder="Masukkan Nama" value="{{ $book->publisher }}">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="year">Tahun</label>
                            <input type="text" name="year" class="form-control" 
                            id="year" placeholder="Masukkan Nama" value="{{ $book->year }}">
                        </div>
                    </div>
                <button type="submit" class="btn btn-danger">Simpan</button>
        </form>
    <!-- end form for validations -->
    </div>
</div>
@endsection
