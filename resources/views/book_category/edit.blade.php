@extends('layouts.template')

@section('content')
<div class="x_panel">
    <div class="x_title">
        <h2>Ubah Data Kategori</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <!-- start form for validation -->
        <form action="{{ route('book_category.update', $category->id) }}" method="post">
            @csrf
            @method('put')
                <div class="form-row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name">Nama</label>
                            <input type="text" name="name" class="form-control" 
                            id="name" placeholder="Masukkan Nama" value="{{ $category->name }}">
                        </div>
                        <div class="form-group">
                            <label for="description">Deskripsi</label>
                            <textarea class="form-control" name="description" 
                            id="description" rows="4">{{ $category->description }}</textarea>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-danger">Simpan</button>
        </form>
    <!-- end form for validations -->
    </div>
</div>
@endsection
