@extends('layouts.template')

@section('content')
<div class="col-md-12 col-sm-6">
  <div class="row">
    <div class="x_panel">
      <div class="x_title">
        <h2>Data Kategori</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li>
              <a href="{{ route('book_category.create') }}">
                <button type="button" class="btn btn-primary btn-sm">Tambah</button>
              </a>
            </li>
          </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table class="table table-striped">
          <thead>
            <tr style="text-align: center;">
                <th scope="col">No</th>
                <th scope="col">Nama</th>
                <th scope="col">Deskripsi</th>
                <th scope="col">Pilihan</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($category as $i => $item)
            <tr style="text-align: center;">
                <td>{{ $i+1 }}</td>
                <td>{{ $item->name }}</td>
                <td>{{ $item->description }}</td>
                <td>
                  <form action="{{ route('book_category.destroy',$item->id) }}" method="post">
                  <a href="{{ route('book_category.edit',$item->id) }}">
                    <button type="button" class="btn btn-success">Ubah</button>
                  </a>
                  @csrf 
                  @method('delete')
                    <button type="submit" class="btn btn-danger" 
                    onclick="return confirm('Yakin ingin dihapus ?')">Hapus</button>
                  </form>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div> 
@endsection
