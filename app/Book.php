<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
	public function categoryRef()
	{
		return $this->belongsTo(BookCategory::class, 'book_category_id');
	}

	public function peminjam()
    {
    	return $this->hasMany(Borrow::class, 'book_id');
    }
}
